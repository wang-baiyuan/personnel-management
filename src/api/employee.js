import request from '@/utils/request'

//获取员工列表
export function getEmployeeList(params){
    return request({
        url: '/sys/user',
        params //地址参数 查询参数
    })
}
//导出员工excel
export function exportEmployee(){
    return request({
        url: '/sys/user/export',
        //改变接收数据类型
        responseType:'blob' //使用blob接收二进制文件流

    })
}
//下载导入员工模板
export function getExportTemplate(){
    return request({
        url: '/sys/user/import/template',
        //改变接收数据类型
        responseType:'blob' //使用blob接收二进制文件流
        
    })
}
//上传用户的excel
export function uploadExcel(data){
    return request({
        url: '/sys/user/import',
        method:'post',
        data //form-data类型
         
    })
}
//删除员工
export function delEmployee(id){
    return request({
        url: `/sys/user/${id}`,
        method:'delete'
    })
}
//新增员工
export function addEmloyee(data){
    return request({
        url:'/sys/user',
        method:'post',
        data
    })
}
//获取员工详情
export function getEmloyeeDetail(id){
    return request({
        url:`/sys/user/${id}`,
    })
}
//更新员工
export function updateEmployee(data){
    return request({
        url:`/sys/user/${data.id}`,
        method:'put',
        data
    })
}
//获取可用角色
export function getEnableRoleList(){
    return request({
        url:'/sys/role/list/enabled',
    })
}
//分配员工角色
export function assignRole(data){
    return request({
        url:'/sys/user/assignRoles',
        method:'put',
        data
    })
}