import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/sys/login',
    method: 'post',
    data
  })
}

export function getUserInfo(){//导出用户资料接口
  return request({
    url:'/sys/profile',
  })

}

export function updatePassword(data){//更新密码
  return request({
    url:'/sys/user/updatePass',
    method:'put',
    data
  })

}



